<footer>
    <div class="footer-outer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                    <div class="footer-content">
                        <div class="ft-address">
                            <p>P7-37.OT12B-15, TÒA NHÀ PARK 7 VINHOMES CENTRAL PARK,</p>
                            <p>720A ĐIỆN BIÊN PHỦ, PHƯỜNG 22, Q. BÌNH THẠNH, TP.HCM, Việt Nam</p>
                            <p>Tel: 19000 68899 / Email: info@liberovina.vn</p>
                            <p><a href="/content/images/publics/profile/Profile.pdf" target="_blank">Hồ sơ công ty</a></p>
                        </div>
                        <div class="ft-line">

                        </div>
                        <div class="ft-social">
                            <ul class="lst-soc">
                                <li class="ft-soc-item">
                                    <a href="https://www.facebook.com/liberovina.vn" target="_blank" class="ft-link-soc">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="ft-soc-item">
                                    <a href="https://www.linkedin.com/in/liberovina-viet-nam-bab10a188/" target="_blank" class="ft-link-soc">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li class="ft-soc-item">
                                    <a href="https://www.pinterest.com/liberovinavn/" target="_blank" class="ft-link-soc">
                                        <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li class="ft-soc-item">
                                    <a href="https://www.instagram.com/liberovina.vn/" target="_blank" class="ft-link-soc">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="ft-line">

                        </div>
                        <div class="coppy-right" style="text-align:center;">
                            <span style="color:#848484; font-size:12px;">Copyright (C) 2017 by Liberovina.vn - All right reserved</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
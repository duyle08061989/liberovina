<header>
	<div class="container container-get-margin">
		<div class="">
			<div class="nav-top">
				<div class="nav-content">
					<div class="nav-btn">
						<span class="open-nav-btn"><span class="icon-bt"></span></span>
					</div>
					<div class="logo">
						<a href="/">
							<img src="/content/images/publics/logo.png" />
						</a>
					</div>
					<div class="b2b-logo">
						<a href="/b2b">
							<img src="/content/images/publics/b2b.png" />
						</a>
					</div>
				</div>
				<div class="sele-lang">
					<div id="google_translate_element"></div>
					<script type="text/javascript">
						function googleTranslateElementInit() {
							new google.translate.TranslateElement({pageLanguage: 'vi'}, 'google_translate_element');
						}
					</script>
					<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
				</div>
			</div>
		</div>
	</div>
	<div id="myNav" class="overlay">
		<div class="container">
			<div class="row">
				<ul class="overlay-content col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
					<li><a href="/">Trang chủ</a></li>
					<li><a href="/WhoWeAre">Giới thiệu</a></li>
					<li><a href="/OurService">Dịch vụ</a></li>
					<li class="li-sub-content">
						<a href="#">Sản phẩm</a>
						<ul class="overlay-sub-content">
							<li><a href="/OurProduct/Diary">Sổ Tay</a></li>
							<li><a href="/OurProduct/DeskAccessories">Văn Phòng Phẩm</a></li>
							<li><a href="/OurProduct/BagWallet">Túi & Ví Da</a></li>
							<li><a href="/OurProduct/LeatherITAccessories">Phụ Kiện Da Cho Sản Phẩm Công Nghệ</a></li>
							<li><a href="/OurProduct/LeatherCarAccessories ">Phụ Kiện Da Cho Xe Hơi</a></li>
							<li><a href="/OurProduct/FoldableTravelGoods">Phụ Kiện Du Lịch Gấp Gọn</a></li>
							<li><a href="/OurProduct/Gift">Sản Phẩm Trang Trí & Quà Tặng Từ Da</a></li>
						</ul>
					</li>
					<li><a href="/OurClient">Đối tác</a></li>
					<li><a href="/Recruitment">Tuyển dụng</a></li>
					<li><a href="/Contact">Liên hệ</a></li>
				</ul>
			</div>
		</div>
	</div>
</header>
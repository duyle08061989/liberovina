<meta charset="utf-8" />
<title>LIBEROVINA | @yield('title')</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="INDEX,FOLLOW" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<link rel="icon" href="/content/images/icons/favicon.png" type="image/x-icon">
{{ csrf_field() }}

<!-- Bootstrap core CSS -->
<link href="{{ URL::to('/') }}/content/bootstrap.css" rel="stylesheet">
<!-- Slick Slider CSS -->
<link href="{{ URL::to('/') }}/content/slick/slick.css" rel="stylesheet"/>
<link href="{{ URL::to('/') }}/content/slick/slick-theme.css" rel="stylesheet"/>
<!-- Fav icon -->
<link rel="icon" type="icon" sizes="96x96" href="{{ URL::to('/') }}/content/images/icons/favicon-192x192.png">
<link href="{{ URL::to('/') }}/content/font-awesome.css" rel="stylesheet">
<link href="{{ URL::to('/') }}/content/css/layout.css" rel="stylesheet">

<script src="{{ URL::to('/') }}/scripts/modernizr-2.6.2.js"></script>
@stack('css')

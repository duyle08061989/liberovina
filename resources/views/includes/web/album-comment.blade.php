@php
    $list_comment = $data->getData();
@endphp


<div class="msl-black">
    <div class="msl-heading light-color">
        <h5><span>{{count($list_comment)}} Bình Luận</span></h5>
    </div>
</div>
<div class="clearfix"></div>
<div id="comments">
    @if (!Auth::check())
        <h6>Vui lòng <a href="#" data-toggle="modal" data-target="#login-register1" style="color:#db152e">đăng nhập</a> để bình luận.</h6>
    @else
    <form class="form-comment" method="POST" action="{{route('comment.album.post')}}">
        @csrf
        <input type="hidden" name="album_id" value="{{$album_id}}">
        <textarea class="form-control" name="content" placeholder="Để lại cảm nhận của bạn..." rows="3" required></textarea>
        <br>
        <button type="submit" class="btn btn-info pull-right">Gửi</button>
    </form>
    @endif
    <div class="clearfix"></div>
    <br>
    <ul class="media-list">
        @if (count($list_comment) > 0)
            @foreach ($list_comment as $comment)
                <li class="media"> 
                    {{-- <a href="#" class="pull-left"> 
                        <img src="https://bootdey.com/img/Content/user_1.jpg" alt="" class="img-circle"> 
                    </a> --}}
                    <div class="media-body"> 
                        <span class="text-muted pull-right"> 
                            <small class="text-muted">{{ date('d/m/Y', $comment->time)}}</small> 
                        </span> 
                        <strong class="user-name">{{$comment->username}}</strong>
                        <p>
                            {{$comment->content}}
                        </p>
                    </div>
                </li>
            @endforeach
        @else
            <li class="media"> 
                <div class="media-body"> 
                    <strong class="user-name">Chưa có bình luận nào!</strong>
                    <p>
                        Hãy là người đầu tiên bình luận cho album này...
                    </p>
                </div>
            </li>
        @endif
    </ul>
</div>
<!--Widget Playlist Start-->
<div class="widget widget-playlist">
    <!--Heading Start-->
    <div class="msl-black">
        <div class="msl-heading light-color">
            <h5><span>Top Platylist</span></h5>
        </div>
    </div>
    <!--Heading End-->
    <div class="widget-player">
        <div class="hide">
            @php
                $topSong = $data->getData();
            @endphp
            @foreach ($topSong as $song)
                <a href="{{env('APP_MEDIA_URL').$song->url}}" title="{{$song->name}}" data-artist="" data-image="/images/cover1.png" data-download="0" data-buy="#">.MP3 file</a><br />
            @endforeach 
            </div>
        <div id="jplayer_jukeboxwidget"></div>
    </div>
</div>
<!--Widget Playlist End-->
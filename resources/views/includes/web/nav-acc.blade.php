<aside>
    <div class="widget widget-artist">
        <div class="msl-black">
            <div class="msl-heading light-color">
                <h5><span>Danh Mục</span></h5>
            </div>
        </div>
        <div class="artists-rank-list">
            <div class="artists-rank block-nav-acc">
               <ul class="nav-acc">
                    <li><a href="#"> Thông tin</a></li>	
                    <li><a href="#"> Tin nhắn</a></li>	
                    <li><a href="{{route('acc.fav.album')}}"> Danh mục yêu thích</a></li>
                    <li><a href="#"> Cài đặt</a></li>
                    <li><a href="#"> Góp ý</a></li>
                    <li><a href="#"> Ngôn ngữ</a></li>
                    <li><a href="#"> Giao diện</a></li>
               </ul>
            </div>
        </div>
    </div>
</aside>

<script>
    (function($){
        $(document).ready(function(){
            var url = window.location.href;   
            $(".nav-acc").find("a[href='"+ url +"']").closest('li').addClass('active');
        });
    })(jQuery);
</script>
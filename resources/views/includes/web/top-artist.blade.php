
@php
    $data = $data->getData();
    $stt = 0;
@endphp
<!--Widget Artist Start-->
<div class="widget widget-artist">
    <!--Heading Start-->
    <div class="msl-black">
        <div class="msl-heading light-color">
            <h5><span>Top 10 Artists</span></h5>
        </div>
    </div>
    <!--Heading End-->
    <!--Artist Rank List Start-->
    <div class="artists-rank-list">
        @foreach ($data as $album)
            <!--Artist Rank End-->
            <div class="artists-rank">
                <figure>
                    <img src="{{env('APP_MEDIA_URL').$album->cover}}" onerror="this.src='/extra-images/black-img/artist1.jpg'" alt="">
                </figure>
                <div class="text-overflow">
                    <h6><a href="{{route('album.details',['name'=>$album->name_ascii,'id'=>$album->id])}}">{{$album->name}}</a></h6>
                    <p><a href="{{route('artist.details',['name'=>$album->artist_name_ascii,'id'=>$album->artist_id])}}">{{$album->artist_name}}</a></p>
                </div>
            </div>
            <!--Artist Rank End-->
        @endforeach 
    </div>
    <!--Artist Rank List End-->
</div>
<!--Widget Artist End-->
<?php
    $folders = $folders->getData();
?>

@if(Auth::check())
    @if($check_album->total_album > 0)
    <div class="block-fav">
        <button class="bt-fav subscribe-wrap theme-bg" style="background-color: #21b384">
            <em><i class="fa fa-check" aria-hidden="true"></i>  Yêu thích</em>
            <span></span>
        </button>
    </div>
        
    @else
        <div class="block-fav">
            <button class="bt-fav subscribe-wrap theme-bg" data-toggle="modal" data-target="#fav-form-add">
                <em>Thêm yêu thích</em>
                <span></span>
            </button>
        </div>
        <!-- Modal -->
        <div id="fav-form-add" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="ms-login-form">
                        @csrf
                        <div class="form-group">
                            <div class="ms-heading2">
                                <h3>Danh sách thư mục của bạn <a href="{{route('acc.fav.album')}}" style="margin-left:20px "><i class="fa fa-arrow-right"></i></a></h3>
                            </div>
                            <div class="input-felid">
                                <select name="lst-folder" id="lst-folder">
                                    <option selected value="-1">-- Chọn thư mục thêm album yêu thích --</option>
                                    <option value="0"> [ + Thêm thư mục ] </option>
                                    @foreach ($folders as $folder)
                                        <option value="{{$folder->id}}"> ----- {{$folder->name}} [ {{$folder->total_album}} album ] ----- </option>
                                    @endforeach
                                </select> 
                                <p style="color:red;font-style: italic" class="alert-folder hidden"></p>
                            </div>
                            <button class="bt-add-album btn-normal2 hidden" type="button">Thêm</button>
                            <br>
                            <br>
                            <div class="block-add-folder hidden">
                                <div class="input-felid">
                                    <label>Thêm mới thư mục</label>
                                    <input id="folder_name" type="text" class="" name="folder_name" placeholder="Nhập tên thư mục..." value=""/>
                                    <span style="color:red;font-style: italic" class="text-alert hidden"></span>
                                </div>
                                <div class="btn-submit">
                                    <button class="bt-add-folder btn-normal2" type="button">Thêm thư mục</button>
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <script>
            (function($){
                $(document).ready(function(){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('input[name="_token"]').val()
                        }
                    });
        
                    var blockFav = $(".block-fav");
                    var btAdd = $(".bt-add-folder");
                    var btAddAlbum = $(".bt-add-album");
                    var inputName = $("#folder_name");
                    var lstFolder = $("#lst-folder");
                    var alert = $(".text-alert");
                    var alertFolder = $(".alert-folder");
        
                    function addFolder(folder_name){
                        $.ajax({
                            url: "{{route("folder.add")}}",
                            type: 'POST',
                            data: { folder_name: folder_name },
                            success: function (data) {
                                if(data){
                                    var items = '<option value="'+ data.id +'"> ----- '+ data.name +' [ '+ data.total_album +' album ] ----- </option>';
                                    lstFolder.append(items).val(data.id).trigger('change');
                                    alertFolder.text('Thêm thư mục thành công!').removeClass('hidden');
                                }
                            },
                            error: function (err) {
                            }
                        });
                    }
        
                    function addAlbumToFolder(album_id,folder_id){
                        $.ajax({
                            url: "{{route("folder.add.album")}}",
                            type: 'POST',
                            data: { 
                                    album_id: album_id,
                                    folder_id: folder_id 
                                },
                            success: function (data) {
                                if(data != 0 && data != 1){
                                    lstFolder.children("option:selected").text(' ----- '+ data.name +' [ '+ data.total_album +' album ] ----- ');
                                    alertFolder.text('Album đã được thêm vào thư mục thành công!').removeClass('hidden');
        
                                    var faved = '<button class="bt-fav subscribe-wrap theme-bg" style="background-color: #21b384"><em><i class="fa fa-check" aria-hidden="true"></i>  Yêu thích</em><span></span></button>';
                                    blockFav.html(faved);
                                }
                                else if(data == 0){
                                    alertFolder.text('Đã tồn tại album trong thư mục này!').removeClass('hidden');
                                }
                                else{
                                    alertFolder.text('Thêm album không thành công!').removeClass('hidden');
                                }
                            },
                            error: function (err) {
        
                            }
                        });
                    }
        
                    inputName.keypress(function(){
                        if($(this).val() != "" || $(this).val() != null){
                            alert.text("").addClass('hidden');
                        }
                    })
        
                    btAdd.click(function(){
                        var name = inputName.val();
                        if(name == "" || name == null){
                            alert.text("Vui lòng nhập tên thư mục!").removeClass('hidden');
                            return;
                        }
                        addFolder(name);
                    })
        
                    btAddAlbum.click(function(){
                        var folderId = lstFolder.val();
                        var albumId = '<?php echo $album_id;?>';
                        if(folderId == 0 || folderId == -1){
                            alertFolder.text("Vui lòng chọn 1 thư mục!").removeClass('hidden');
                            return;
                        }
                        addAlbumToFolder(albumId,folderId);
                    })
        
                    lstFolder.change(function(){
                        var _this = $(this);
                        var value = _this.val();
                        if(value == 0){
                            btAddAlbum.addClass('hidden');
                            alertFolder.text("").addClass('hidden');
                            $(".block-add-folder").removeClass('hidden');
                        }
                        else{
                            $(".block-add-folder").addClass('hidden');
                            if(value != -1){
                                alertFolder.text("").addClass('hidden');
                                btAddAlbum.removeClass('hidden');
                            }
                        }
                    })
                })
            })(jQuery);
        </script>
    @endif
@else
    <div class="block-fav">
        <button class="bt-fav subscribe-wrap theme-bg" data-toggle="modal" data-target="#login-register1">
            <em>Thêm yêu thích</em>
            <span></span>
        </button>
    </div>
@endif

@php
    $history = $history->getData();
@endphp
<div class="widget widget-artist">
<div class="msl-black">
    <div class="msl-heading light-color">
        <h5><span>Album Đang Được Nghe</span></h5>
    </div>
</div>
<div class="block-history artists-rank-list">
    <div class="artists-rank">
           <ul class="lst-history">
               @foreach ($history as $h)
                   <li>
                       {!! $h->content !!}
                   </li>
               @endforeach
           </ul>
    </div>
</div>
</div>


<script>
    (function($){
        $(document).ready(function(){
            var lstHistory = $(".lst-history");
            var connection = new WebSocket('wss://{{env("SOCKET_URL")}}:1337');
            connection.onopen = function () {
                console.log("Connected server!");

                var user_name = $("#username").val();
                var album_id = $("#album_id").val();
                var album_name = $("#album_name").val();
                var name_ascii = $("#name_ascii").val();
                var album_cover = $("#album_cover").val();

                if(album_id && album_name && name_ascii && album_cover){
                    var obj = {
                        "user_name": user_name,
                        "album": album_name,
                        "album_id": album_id,
                        "cover": album_cover,
                        "url": '/album/'+ album_id +'/'+ name_ascii +'.html',
                    };
                    connection.send(JSON.stringify(obj))
                }
            };
            connection.onmessage = function (message) {    
                var response = JSON.parse(message.data);
                var history = '';
                /* console.log(response); */
                history +='<li><a href="'+ response.url +'"><span class="username">'+ response.user_name +'</span> đang nghe <span class="album-name">'+ response.album +'</span></a></li>';             
                lstHistory.prepend(history);
             };

            connection.onerror = function (error) {
                console.log(error);
            };
        });
    })(jQuery);
</script>
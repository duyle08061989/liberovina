@extends('layouts.layout_web')

@section('title', 'LIBEROVINA CO.,LTD')

@push('css')
    <link href="{{ URL::to('/') }}/content/css/wwa.css" rel="stylesheet">
@endpush

@section('content')
<div class="body-content">
        
    <div class="main-slide-item">
        <div class="block-breadcrumb">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumb">
                        <li>
                            <a href="/">Trang chủ</a>
                        </li>
                        <li class="active">
                            Giới thiệu
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="slide-item-bg">
            <img style="width:100%;" src="/content/images/publics/Banners/Eblouir-coverFB-2.jpg" />
        </div>
        <div class="container">
            <div class="">
                
            </div>
        </div>
    </div>
    <div class="wwa-body">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        <h5>giới thiệu</h5>
                        <h1>
                            về chúng tôi
                        </h1>
                    </div>
                    <img class="img-responsive" src="/content/images/publics/profile/Slide17.PNG" /><br/><br/>
                    <img class="img-responsive" src="/content/images/publics/profile/Slide18.PNG" /><br/><br/>
                    <img class="img-responsive" src="/content/images/publics/profile/Slide19.PNG" /><br/><br/>
                    <img class="img-responsive" src="/content/images/publics/profile/Slide20.PNG" /><br/><br/>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <br />


@endsection

@push('scripts')
    <script>
        (function($){
            $(document).ready(function(){
                $(".list-emp").slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    rows:2,
                    infinite: true,
                    autoplay: true,
                    autoplaySpeed: 3000,
                    responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 479,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                        }
                    }
                    ]
                });
            });
        })(jQuery);  
    </script>
@endpush
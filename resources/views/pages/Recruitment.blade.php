@extends('layouts.layout_web')

@section('title', 'LIBEROVINA CO.,LTD')

@push('css')
    <link href="{{ URL::to('/') }}/content/css/ourclient.css" rel="stylesheet">
@endpush

@section('content')
<div class="main-slide-item">
        <div class="block-breadcrumb">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumb">
                        <li>
                            <a href="/">Trang chủ</a>
                        </li>
                        <li>
                            Tuyển Dụng
                        </li>
                        <li class="active">
                            SALES ADMIN
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="slide-item-bg">
            <img src="/content/images/publics/Banners/banner-pro-vpp.png" />
        </div>
        <div class="container">
            <div class="">
                <div class="slide-desc-outer">
                    <div class="slide-desc">
                        <h4><span style="font-family: Lato-Light, Arial, sans-serif;text-transform:uppercase;color:#fff;font-size:16px;">cung cấp giải pháp tốt hơn</span><span style="font-family: Lato-Bold, Arial, sans-serif;text-transform:uppercase;font-size:30px;color:#b8a27e"> cho các công ty</span></h4>
                        <h5><span style="font-family: Lato-Black,Arial,sans-serif;text-transform:uppercase;font-size:48px;color:#fff">thương mại điện tử</span></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="client-body container">
        <div class="">
            <div class="contact-header col-md-6 col-sm-9">
                <div class="header">
                    <h5>Libero Vina</h5>
                    <h1>
                         Tuyển dụng vị trí Sales Admin
                    </h1>
                </div>
                <div class="content">
                    <div>
                        Libero là doanh nghiệp chuyên sản xuất và phân phối các dòng phụ kiện du lịch, bao da chìa khóa dành cho xe Ô Tô và các ản Phẩm Chăm sóc sức khỏe. Với hơn 40 năm kinh nghiệm sản xuất tại Hàn Quốc, Trung Quốc và gần đây nhất là Việt Nam.
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="">
            <div class="col-sm-12">
                <div class="row">
                    <div class="block-slider">
                        <button class="accordion active">1. Mô Tả Công Việc Sales Admin</button>
                        <div class="panel">
                            <ul>
                                <li>
                                    Họp và ghi chú nội dung cuộc họp.
                                </li>
                                <li>
                                    Tổng hợp báo cáo phòng kinh doanh để theo dõi công việc hàng ngày.
                                </li>
                                <li>
                                    Hỗ trợ Phòng Kinh Doanh làm đơn đặt hàng, theo dõi đơn đặt hàng.
                                </li>
                                <li>
                                    Tổng hợp báo cáo và báo cáo trực tiếp Ban Giám Đốc hàng tuần.
                                </li>
                            </ul>
                        </div>
    
                        <button class="accordion">2. Yêu cầu công việc</button>
                        <div class="panel">
                            <ul>
                                <li>
                                    Kinh nghiệm tối thiểu 1 năm ở vị trí tương đương.
                                </li>
                                <li>
                                    Thành thạo về Ms Office.
                                </li>
                                <li>
                                    Ưu tiên các ứng viên có kinh nghiệm và thành thạo Tiếng Anh là một lợi thế.
                                </li>
                                <li>
                                    Khả năng làm việc và trao đổi với các bộ phận Kho, Điều Phối, Kế Toán, Marketing, …
                                </li>
                            </ul>
                        </div>
    
                        <button class="accordion">3. Lương và Phúc lợi</button>
                        <div class="panel">
                            <ul>
                                <li>
                                    Mức lương: Thương Lượng tùy thuộc vào kinh nhiệm.
                                </li>
                                <li>
                                    Phúc lợi: hưởng đầy đủ phúc lợi theo quy định luật pháp VN như BHXH, Du lịch,…
                                </li>
                                <li>
                                    Được đào tạo và học hỏi thêm trong quá trình làm việc.
                                </li>
                            </ul>
                        </div>
    
                        <pre>
                            Mọi thông tin khác vui lòng liên lạc về: 
                            Email: hr@liberovina.vn 
                            Hotline: 19000 68899 - 0344904810- Ms Tuyền HR     
                        </pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <br />
    
            </div>   


@endsection

@push('scripts')
    <script>
        (function($){
            $(document).ready(function(){
                var acc = document.getElementsByClassName("accordion");
                var i;

                for (i = 0; i < acc.length; i++) {
                    acc[i].addEventListener("click", function () {
                        this.classList.toggle("active");
                        var panel = this.nextElementSibling;
                        if (panel.style.maxHeight) {
                            panel.style.maxHeight = null;
                        } else {
                            panel.style.maxHeight = panel.scrollHeight + "px";
                        }
                    });
                }
            });
        })(jQuery);  
    </script>
@endpush
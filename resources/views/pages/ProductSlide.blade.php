@if ($type == 1)
<div class="slide-pro">
    <div class="slide-item">
        <img class="img-responsive" src="/content/images/publics/Products/pro-01.png" />
        <div class="title-slide">
            <h4>businessman gifts set</h4>
            <p>designed and developed</p>
        </div>
    </div>
    <div class="slide-item">
        <img class="img-responsive" src="/content/images/publics/Products/pro-02.png" />
        <div class="title-slide">
            <h4>travel gifts set</h4>
            <p>designed and developed</p>
        </div>
    </div>
    <div class="slide-item">
        <img class="img-responsive" src="/content/images/publics/Products/pro-04.png" />
        <div class="title-slide">
            <h4>Stationery design</h4>
            <p>designed and developed</p>
        </div>
    </div>
</div>
@endif
@if ($type == 2)
<div class="slide-pro">
    <div class="slide-item">
        <img class="img-responsive" src="/content/images/publics/Products/pro-03.png" />
        <div class="title-slide">
            <h4>Emarketplace</h4>
            <p>designed and developed</p>
        </div>
    </div>
    <div class="slide-item">
        <img class="img-responsive" src="/content/images/publics/Products/pro-05.png" />
        <div class="title-slide">
            <h4>website design</h4>
            <p>designed and developed</p>
        </div>
    </div>
    <div class="slide-item">
        <img class="img-responsive" src="/content/images/publics/Products/pro-06.png" />
        <div class="title-slide">
            <h4>website design</h4>
            <p>designed and developed</p>
        </div>
    </div>
</div>
@endif
@if ($type == 3)
<div class="slide-pro">
    <div class="slide-item">
        <img class="img-responsive" src="/content/images/publics/Products/pro-07.png" />
        <div class="title-slide">
            <h4>fashion App mobile</h4>
            <p>designed and developed</p>
        </div>
    </div>
    <div class="slide-item">
        <img class="img-responsive" src="/content/images/publics/Products/pro-08.png" />
        <div class="title-slide">
            <h4>furniture App mobile</h4>
            <p>designed and developed</p>
        </div>
    </div>
</div>
@endif


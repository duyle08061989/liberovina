@extends('layouts.layout_web')

@section('title', 'LIBEROVINA CO.,LTD')

@push('css')
    <link href="{{ URL::to('/') }}/content/css/home.css" rel="stylesheet">
@endpush

@section('content')
<div class="body-content">
    <section class="main-slider-outer">
        <div class="main-slide">
            
            <div class="main-slide-item">
                <div class="slide-item-bg">
                    <img src="/content/images/publics/Banners/Eblouir-coverFB-2c.jpg" />
                </div>
                <div class="container">
                    <div class="row">
                        <div class="slide-desc-outer col-lg-4 col-md-5 col-sm-7">
                            <div class="slide-desc">
                                <h4><span style="color:#b8a27e">MAKE </span><span style="font-family: OpenSans-ExtraBold, Arial, sans-serif;color:#fff;text-transform:uppercase;font-weight:600"> the e-commerce company</span></h4>
                                <h1>
                                    Feel better
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-slide-item">
                <div class="slide-item-bg">
                    <img src="/content/images/publics/Banners/Eblouir-coverFB-1.jpg" />
                </div>
                <div class="container">
                    <div class="row">
                        <div class="slide-desc-outer col-lg-4 col-md-5 col-sm-7">
                            <div class="slide-desc">
                                <h4 style="font-family: Lato-Light, Arial, sans-serif;text-transform:uppercase"><span style="color:#fff"> we </span><span style="color:#b8a27e"> create</span></h4>
                                <h3 style="font-family: Lato-Black, Arial, sans-serif;text-transform:uppercase;font-weight:600;font-size:46px;color:#fff"><span style="color:#fff">new trends to</span></h3>
                                <h5><span style="font-family: Lato-Light,Arial,sans-serif;text-transform:uppercase;font-size:18px;color:#f5e095;letter-spacing:8px">keep your moments</span></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-slide-item">
                <div class="slide-item-bg">
                    <img src="/content/images/publics/Banners/Eblouir-coverFB-2a.jpg" />
                </div>
                <div class="container">
                    <div class="row">
                        <div class="slide-desc-outer col-lg-4 col-md-5 col-sm-7">
                            <div class="slide-desc">
                                <h4 style="font-family: Lato-Light, Arial, sans-serif;text-transform:uppercase"><span style="color:#fff"> we </span><span style="font-family: OpenSans-ExtraBold, Arial, sans-serif;color:#b8a27e;"> make</span></h4>
                                <h3 style="font-family: Lato-Black, Arial, sans-serif;text-transform:uppercase;font-weight:600;font-size:36px;color:#fff"><span style="color:#fff">new everythings for</span></h3>
                                <h5><span style="font-family: Lato-Light,Arial,sans-serif;text-transform:uppercase;font-size:18px;color:#f5e095;letter-spacing:8px">your shopping online</span></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="who-we-are-block">
        <div class="wwa-content-outer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="wwa-content">
                            <div class="header">
                                <h5>giới thiệu</h5>
                                <h1>
                                    Về Chúng Tôi
                                </h1>
                            </div>
                            <div class="content">
                                <h5>
                                    Liberovina là một doanh nghiệp trẻ về thương mại điện tử tại thị trường Việt Nam.
                                </h5>
                                <div>
                                    Với mong muốn mang đến sự thành công cho các đối tác của Liberovina, chúng tôi luôn cố gắng tạo ra nguồn cảm hứng cũng như sự tin cậy tốt nhất về sản phẩm và chất lượng sản phẩm cho khách hàng doanh nghiệp lẫn người tiêu dùng Việt.
                                </div>
                                <a class="read-more wwa-read-more" href="/WhoWeAre">
                                    <span class="plush">+</span><span class="bt-text">Xem Thêm</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="img-centrer col-lg-4 col-md-6 col-sm-6 hidden-xs">
                        <div class="img-elm">
                            <img src="/content/images/publics/about-1.png" />
                        </div>
                    </div>
                    <div class="img-right col-lg-4 hidden-md hidden-sm hidden-xs">
                        <div class="img-elm">
                            <img src="/content/images/publics/about-2.png" />
                        </div>
                        <div class="img-elm">
                            <img src="/content/images/publics/about-3.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>
    <section class="our-service-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="header">
                        <h5>Dịch Vụ</h5>
                        <h1>
                            Của Chúng Tôi
                        </h1>
                    </div>
                    <div class="content">
                        <h5>
                            Liberovina cung cấp dịch vụ về thương mại điện tử, thiết kế quà tặng cho doanh nghiệp tại thị trường Việt Nam.
                        </h5>
                    </div>
                </div>
            </div>
            <div class="our-service-content">
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="item">
                                <div class="item-header">
                                    <div class="icon">
                                        <img src="/content/images/icons/sv-icon-01.png" alt="Libero Vina" />
                                    </div>
                                    <div class="title">
                                        <h4>Thiết Kế Sáng Tạo</h4>
                                    </div>
                                </div>
                                <div class="item-line"></div>
                                <div class="item-content">
                                    Chúng tôi có một đội ngũ nhân viên IT trẻ, có khả năng thiết kế và xây dựng trang web theo yêu cầu của đối tác, từ Website cho đến App mobile thuộc các lĩnh vực khác nhau.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="item">
                                <div class="item-header">
                                    <div class="icon">
                                        <img src="/content/images/icons/sv-icon-02.png" alt="Libero Vina" />
                                    </div>
                                    <div class="title">
                                        <h4>Lĩnh Vực Quà tặng</h4>
                                    </div>
                                </div>
                                <div class="item-line"></div>
                                <div class="item-content">
                                    Liberovina chúng tôi hiện đang cung cấp các sản phẩm quà tặng dành cho các hoạt động tập thể, các sự kiện của doanh nghiệp hay cơ quan. Hơn nữa, hiện chúng tôi cũng có cơ sở sản xuất có thể sản xuất theo nhu cầu của quý khách hàng.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="item">
                                <div class="item-header">
                                    <div class="icon">
                                        <img src="/content/images/icons/sv-icon-03.png" alt="Libero Vina" />
                                    </div>
                                    <div class="title">
                                        <h4>Thương mại Điện Tử</h4>
                                    </div>
                                </div>
                                <div class="item-line"></div>
                                <div class="item-content">
                                    Mô hình thương mại điện tử của Liberovina sẽ tối đa hoá sự tiếp xúc giữa sản phẩm và khách hàng, thông qua việc thúc đẩy hoạt động mua bán sản phẩm và thu hút sự tham gia của người tiêu dùng từ đó đẩy mạnh việc tăng doanh số của các nhà bán hàng.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="read-more-wrap">
                    <a class="read-more wwa-read-more" href="/OurService">
                        <span class="plush">+</span><span class="bt-text">Xem Thêm</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="product-block">
        <div class="container">
            <div class="row">
                <div class="block-left col-md-4">
                    <div class="header">
                        <h5>sản phẩm</h5>
                        <h1>
                            Chúng tôi thực hiện
                        </h1>
                    </div>
                    <div class="content">
                        <h5>
                            Liberovina có giải pháp tốt nhất về thương mại điện tử và thiết kế quà tặng cho doanh nghiệp tại thị trường Việt Nam.
                        </h5>
                    </div>
                    <div class="nav-ctg-products">
                        <ul class="lst-ctg">
                            <li class="active" tab-id="1">Thiết kế quà tặng</li>
                            <li tab-id="2">Design và sản xuất</li>
                            <li tab-id="3">Bán và phân phối qua hệ thống E-commerce</li>
                        </ul>
                    </div>
                    <div class="read-more-wrap">
                        
                    </div>
                </div>
                <div class="block-right col-md-7 col-md-offset-1">
                    <!-- Tab content -->
                    <div class="tabcontent active">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="our-brand-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header">
                        <h5>Thương Hiệu</h5>
                        <h1>
                            Của Chúng Tôi
                        </h1>
                    </div>
                    <div class="content">
                        <h5>
                            <p>Libero Vina sở hữu 3 thương hiệu phục vụ bán lẻ:</p>
                            <ul>
                                <li><a href="https://anse.vn" target="_blank" style="color: #333333"><strong>Anse</strong> - phụ kiện du lịch</a></li>  
                                <li><a href="https://eblouir.vn" target="_blank" style="color: #333333"><strong>Eblouir</strong> - phụ kiện thời trang bằng da thật</a></li>  
                                <li><a href="https://mediton.vn" target="_blank" style="color: #333333"><strong>Mediton</strong> - túi chườm gel giữ nhiệt</a></li> 
                            </ul>
                            <br>
                            Đồng thời Libero Vina cũng cung cấp dịch vụ thiết kế, sản xuất quà tặng cho doanh nghiệp và gia công sản phẩm cho các thương hiệu tại thị trường Việt Nam.
                        </h5>
                    </div>
                </div>
            </div>
            <div class="our-service-content">
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="item">
                                <div class="item-header">
                                    <a class="icon" href="https://anse.vn" target="_blank">
                                        <img class="img-responsive" src="/content/images/anse.jpg" alt="Anse" />
                                    </a>
                                    <div class="title">
                                        <h4><a href="https://anse.vn" target="_blank" rel="noopener noreferrer">ANSE</a></h4>
                                    </div>
                                </div>
                                <div class="item-line"></div>
                                <div class="item-content">
                                    Phụ Kiện Du Lịch <br>
                                    Balo, Túi Xách Gấp Gọn
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="item">
                                <div class="item-header">
                                    <a class="icon" href="https://eblouir.vn" target="_blank">
                                        <img class="img-responsive" src="/content/images/eblouir.jpg" alt="Eblouir" />
                                    </a>
                                    <div class="title">
                                        <h4><a href="https://eblouir.vn" target="_blank" rel="noopener noreferrer">EBLOUIR</a></h4>
                                    </div>
                                </div>
                                <div class="item-line"></div>
                                <div class="item-content">
                                    Bao Da Chìa Khóa Xe Hơi <br>
                                    Bóp, Ví Da
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="item">
                                <div class="item-header">
                                    <a class="icon" href="https://mediton.vn" target="_blank">
                                        <img class="img-responsive" src="/content/images/mediton.jpg" alt="Mediton" />
                                    </a>
                                    <div class="title">
                                        <h4><a href="https://mediton.vn" target="_blank" rel="noopener noreferrer">MEDITON</a></h4>
                                    </div>
                                </div>
                                <div class="item-line"></div>
                                <div class="item-content">
                                    Túi Chườm Nóng Lạnh Dạng Gel <br>
                                    Mặt Nạ Mắt
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonials">
        <div class="container">
            <div class="row">
                <div class="tmn-slide col-md-10 col-md-offset-1 col-sm-12">
                    <div class="tmn-item col-md-4">
                        <img class="icon-comma" src="/content/images/icons/icon-10.png" />
                        <p class="tmn-content">Whatever you are doing on the website, keep it up, cos enquiries are flooding in; keep pressing the button and keep us on page one.</p>
                        <div class="avatar">
                            <img src="/content/images/icons/icon-11.png" />
                            <p class="cust-name">Molly Michael</p>
                            <p class="cust-position">Everline, Product Manager</p>
                        </div>
                    </div>
                    <div class="tmn-item col-md-4">
                        <img class="icon-comma" src="/content/images/icons/icon-10.png" />
                        <p class="tmn-content">Whatever you are doing on the website, keep it up, cos enquiries are flooding in; keep pressing the button and keep us on page one.</p>
                        <div class="avatar">
                            <img src="/content/images/icons/icon-11.png" />
                            <p class="cust-name">Molly Michael</p>
                            <p class="cust-position">Everline, Product Manager</p>
                        </div>
                    </div>
                    <div class="tmn-item col-md-4">
                        <img class="icon-comma" src="/content/images/icons/icon-10.png" />
                        <p class="tmn-content">Whatever you are doing on the website, keep it up, cos enquiries are flooding in; keep pressing the button and keep us on page one.</p>
                        <div class="avatar">
                            <img src="/content/images/icons/icon-11.png" />
                            <p class="cust-name">Molly Michael</p>
                            <p class="cust-position">Everline, Product Manager</p>
                        </div>
                    </div>
                    <div class="tmn-item col-md-4">
                        <img class="icon-comma" src="/content/images/icons/icon-10.png" />
                        <p class="tmn-content">Whatever you are doing on the website, keep it up, cos enquiries are flooding in; keep pressing the button and keep us on page one.</p>
                        <div class="avatar">
                            <img src="/content/images/icons/icon-11.png" />
                            <p class="cust-name">Molly Michael</p>
                            <p class="cust-position">Everline, Product Manager</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
@endsection

@push('scripts')   
    <script>
        (function($){
            
            $(document).ready(function(){
                
            });
        })(jQuery);  
    </script>
    <script src="{{ URL::to('/') }}/content/js/home.js"></script>
@endpush
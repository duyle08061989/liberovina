@extends('layouts.layout_web')

@section('title', 'LIBEROVINA CO.,LTD')

@push('css')
    <link href="{{ URL::to('/') }}/content/css/ourclient.css" rel="stylesheet">
@endpush

@section('content')
<div class="body-content">
            



        <div class="main-slide-item">
            <div class="block-breadcrumb">
                <div class="container">
                    <div class="row">
                        <ul class="breadcrumb">
                            <li>
                                <a href="/">Trang chủ</a>
                            </li>
                            <li>
                                Sản phẩm
                            </li>
                            <li class="active">
                                Phụ Kiện Du Lịch Gấp Gọn
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="slide-item-bg">
                <img src="/content/images/publics/Banners/bg-products.png" />
            </div>
            <div class="container">
                <div class="">
                    <div class="slide-desc-outer">
                        <div class="slide-desc">
                            <h4><span style="font-family: Lato-Light, Arial, sans-serif;text-transform:uppercase;color:#fff;font-size:16px;">cung cấp giải pháp tốt hơn</span><span style="font-family: Lato-Bold, Arial, sans-serif;text-transform:uppercase;font-size:30px;color:#b8a27e"> cho các công ty</span></h4>
                            <h5><span style="font-family: Lato-Black,Arial,sans-serif;text-transform:uppercase;font-size:48px;color:#fff">thương mại điện tử</span></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="client-body container">
            <div class="">
                <div class="contact-header col-md-6 col-sm-9">
                    <div class="header">
                        <h5>sản phẩm</h5>
                        <h1>
                            Phụ Kiện Du Lịch Gấp Gọn
                        </h1>
                    </div>
                    <div class="content">
                        <div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="block-slider">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/travel_01.png" />
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/travel_02.jpg" />
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/travel_03.jpg" />
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/travel_04.png" />
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/travel_05.png" />
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/travel_06.png" />
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/travel_07.jpg" />
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/travel_08.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <br />
        <br />
    
    
    
            </div>


@endsection

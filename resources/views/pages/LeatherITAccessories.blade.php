@extends('layouts.layout_web')

@section('title', 'LIBEROVINA CO.,LTD')

@push('css')
    <link href="{{ URL::to('/') }}/content/css/ourclient.css" rel="stylesheet">
@endpush

@section('content')
<div class="body-content">
            



        <div class="main-slide-item">
            <div class="block-breadcrumb">
                <div class="container">
                    <div class="row">
                        <ul class="breadcrumb">
                            <li>
                                <a href="/">Trang chủ</a>
                            </li>
                            <li>
                                Sản phẩm
                            </li>
                            <li class="active">
                                Phụ Kiện Da Cho Sản Phẩm Công Nghệ
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="slide-item-bg">
                <img style="width:100%;" src="/content/images/publics/Banners/Eblouir-coverFB-2.jpg">
            </div>
            <div class="container">
                <div class="">
                   
                </div>
            </div>
        </div>
        <div class="client-body container">
            <div class="">
                <div class="contact-header col-md-6 col-sm-9">
                    <div class="header">
                        <h5>sản phẩm</h5>
                        <h1>
                            Phụ Kiện Da Cho Sản Phẩm Công Nghệ
                        </h1>
                    </div>
                    <div class="content">
                        <div>
                            Nhằm mục đích đáp ứng nhu cầu của người tiêu dùng, chúng tôi còn mong muốn đưa ra các sản phẩm dẫn đầu xu hướng trong thị trường hiện nay.
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="block-slider">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/IT_01.jpg">
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/IT_02.jpg">
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/IT_03.jpg">
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/IT_04.jpg">
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/IT_05.jpg">
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/IT_06.jpg">
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/IT_07.jpg">
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/IT_08.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
    
    
    
            </div>


@endsection

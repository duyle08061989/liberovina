@extends('layouts.layout_web')

@section('title', 'LIBEROVINA CO.,LTD')

@push('css')
    <link href="{{ URL::to('/') }}/content/css/ourclient.css" rel="stylesheet">
@endpush

@section('content')
<div class="main-slide-item">
        <div class="block-breadcrumb">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumb">
                        <li>
                            <a href="/">Trang chủ</a>
                        </li>
                        <li>
                            Sản phẩm
                        </li>
                        <li class="active">
                            Phụ kiện kỹ thuật số
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="slide-item-bg">
            <img src="/content/images/publics/Banners/banner-pro-kts.png" />
        </div>
        <div class="container">
            <div class="">
                <div class="slide-desc-outer">
                    <div class="slide-desc">
                        <h4><span style="font-family: Lato-Light, Arial, sans-serif;text-transform:uppercase;color:#fff;font-size:16px;">cung cấp giải pháp tốt hơn</span><span style="font-family: Lato-Bold, Arial, sans-serif;text-transform:uppercase;font-size:30px;color:#b8a27e"> cho các công ty</span></h4>
                        <h5><span style="font-family: Lato-Black,Arial,sans-serif;text-transform:uppercase;font-size:48px;color:#fff">thương mại điện tử</span></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="client-body container">
        <div class="">
            <div class="contact-header col-md-6 col-sm-9">
                <div class="header">
                    <h5>sản phẩm</h5>
                    <h1>
                        Sổ Tay
                    </h1>
                </div>
                <div class="content">
                    <div>
                        Dành cho mục đích làm quà tặng hay khách hàng mua sỉ số lượng lớn.
                        Tùy theo mục đích mua hàng, chúng tôi bảo đảm sẽ làm hài lòng quý khách hàng bằng dịch vụ, sản phẩm mới lại của công ty.
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="">
            <div class="col-sm-12">
                <div class="row">
                    <div class="block-slider">
                        <div class="col-md-3 col-sm-4 col-xs-6">
                            <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/diary_01.jpg" />
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-6">
                            <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/diary_02.jpg" />
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-6">
                            <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/diary_03.jpg" />
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-6">
                            <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/diary_04.jpg" />
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-6">
                            <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/diary_05.jpg" />
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-6">
                            <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/diary_06.jpg" />
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-6">
                            <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/diary_07.jpg" />
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-6">
                            <img class="img-responsive margin-bottom-30px" src="/content/images/publics/SampleProducts/diary_08.jpg" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <br />



        </div>


@endsection

@push('scripts')
    <script src="{{ URL::to('/') }}/content/js/home.js"></script>
    <script>
        (function($){
            $(document).ready(function(){

            });
        })(jQuery);  
    </script>
@endpush
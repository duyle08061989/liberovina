@extends('layouts.layout_web')

@section('title', 'LIBEROVINA CO.,LTD')

@push('css')
    <link href="{{ URL::to('/') }}/content/css/service.css" rel="stylesheet">
@endpush

@section('content')
<div class="main-slide-item">
        <div class="block-breadcrumb">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumb">
                        <li>
                            <a href="/">Trang chủ</a>
                        </li>
                        <li class="active">
                            Dịch vụ
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="slide-item-bg">
            <img src="/content/images/publics/Banners/bg-services.png" />
        </div>
        <div class="container">
            <div class="">
                <div class="slide-desc-outer">
                    <div class="slide-desc">
                        <h4><span style="font-family: Lato-Light, Arial, sans-serif;text-transform:uppercase;color:#fff;font-size:16px;">cung cấp giải pháp tốt hơn</span><span style="font-family: Lato-Bold, Arial, sans-serif;text-transform:uppercase;font-size:30px;color:#b8a27e"> cho các công ty</span></h4>
                        <h5><span style="font-family: Lato-Black,Arial,sans-serif;text-transform:uppercase;font-size:48px;color:#fff">thương mại điện tử</span></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="service-body container">
        <div class="row">
            <div class="contact-header col-md-6 col-sm-9">
                <div class="header">
                    <h5>dịch vụ</h5>
                    <h1>
                        Của chúng tôi
                    </h1>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="row">
            <div class="service-item">
                <div class="col-md-7 col-sm-6">
                    <div class="service-name">
                        <h2>Business to Business (B2B)</h2>
                    </div>
                    <h4 class="service-title">
                        Chúng tôi hướng khách hàng đến với các dịch vụ thương mại điện tử bắt đầu từ quá trình lên kế hoạch sản phẩm, sản xuất, tìm kiếm nguồn hàng cho đến khâu cung cấp sản phẩm đến người tiêu dùng.
                    </h4>
                    <div class="service-title">
                        <p>
                            Như là một đối tác kinh doanh thành công của các nhà cung cấp và đại lý bằng cách cung cấp các chủng loại sản phẩm.
                            Chúng tôi tạo ra các tuyến bán hàng mới đồng thời đưa ra các giải pháp quản lý theo mô hình Maketplace.
                        </p>
                    </div>
                    <div class="">
                        <a href="/b2b" class="bt-send read-more wwa-read-more"><span class="plush">+</span><span class="bt-text">Xem thêm</span></a>
                    </div>
                </div>
                <div class="col-md-5 col-sm-6">
                    <img class="service-img" src="/content/images/publics/b2b.jpg" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="service-item">
                <div class="col-md-7 col-sm-6">
                    <div class="service-name">
                        <h2>Thương mại điện tử mà chúng tôi đang hướng đến là yếu tố con người</h2>
                    </div>
                    <h4 class="service-title">
                        Chúng tôi hướng khách hàng đến với các dịch vụ thương mại điện tử bắt đầu từ quá trình lên kế hoạch sản phẩm, sản xuất, tìm kiếm nguồn hàng cho đến khâu cung cấp sản phẩm đến người tiêu dùng.
                    </h4>
                    <div class="service-title">
                        <p>
                            Như là một đối tác kinh doanh thành công của các nhà cung cấp và đại lý bằng cách cung cấp các chủng loại sản phẩm.
                            Chúng tôi tạo ra các tuyến bán hàng mới đồng thời đưa ra các giải pháp quản lý theo mô hình Maketplace.
                        </p>
                    </div>
                    
                </div>
                <div class="col-md-5 col-sm-6">
                    <img class="service-img" src="/content/images/publics/TMDT.jpg" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="service-item">
                <div class="col-md-7 col-sm-6">
                    <div class="service-name">
                        <h2>VIETNAM E-COMMERCE PLATFORM</h2>
                    </div>
                    <h4 class="service-title">
                        Thương mại điện tử của Libero Vina đang rút ngắn khoảng cách giữa khách hàng và sản phẩm, tăng doanh số bằng cách tăng số lượng khách hàng và thúc đẩy mua sản phẩm.
                    </h4>
                    <div class="service-title">
                        <p>
                            Chúng tôi cung cấp các dịch vụ thương mại điện tử quảng cáo theo định hướng khách hàng từ lập kế hoạch sản phẩm, sản xuất, tìm nguồn cung ứng cho đến quy trình cung ứng.
                            Là một đối tác kinh doanh thành công của các nhà cung cấp và đại lý cung cấp các loại sản phẩm khác nhau, chúng tôi tạo ra các tuyến bán hàng mới và các giải pháp quản lý dựa trên kinh doanh thị trường mở.
                        </p>
                        <p>
                            Chúng tôi sẽ tạo ra một mô hình mới về thương mại điện tử thông qua một nền tảng hướng đến người dùng.
                        </p>
                    </div>
                    <div class="service-tag">
                        <p>e-commerce system / B2B2C / development /</p>
                    </div>
                </div>
                <div class="col-md-5 col-sm-6">
                    <img class="service-img" src="/content/images/publics/service-03.png" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <div class="service-item">
                <div class="col-md-7 col-sm-6">
                    <div class="service-name">
                        <h2>SẢN XUẤT VÀ PHÂN PHỐI QUÀ TẶNG DOANH NGHIỆP</h2>
                    </div>
                    <h4 class="service-title">
                        Chúng tôi sản xuất và phân phối các sản phẩm quà tặng doanh nghiệp nhằm đáp ứng nhu cầu của các doanh nghiệp và của các cơ quan đoàn thể.
                    </h4>
                    <div class="service-title">
                        <p>
                            Các sản phẩm quà tặng của chúng tôi luôn mang lại hiệu quả cao cho các sự kiện và chiến dịch quảng bá của các công ty cơ quan, đoàn thể.Không chỉ dừng lại ở đó, cơ sở sản xuất của chúng tôi còn có thể đáp ứng được mọi yêu cầu sản xuất của khách hàng.
                            Đối với các loại sổ tay, đồ da, ốp điện thoại thông minh,…chúng tôi có thể cung cấp sỉ các loại sản phẩm quà tặng qua việc nhập khẩu và tìm kiếm nguồn cung ứng từ nhiều quốc gia trên thế giới.
                        </p>
                        <p>
                            Là một đối tác kinh doanh thành công, không chỉ với các sản phẩm quà tặng, chúng tôi còn có thể cung cấp sỉ nhiều loại hình sản phẩm đa dạng.
                        </p>
                    </div>
                    <div class="service-tag">
                        <p>provider gift / events / gift design /</p>
                    </div>
                </div>
                <div class="col-md-5 col-sm-6">
                    <img class="service-img" src="/content/images/publics/service-02.png" />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="service-item">
                <div class="col-md-7 col-sm-6">
                    <div class="service-name">
                        <h2>Thiết Kế Sáng Tạo</h2>
                    </div>
                    <h4 class="service-title">
                        Thiết kế sáng tạo cho các sản phẩm đạt tiêu chuẩn website Thương Mại Điện Tử và bắt kịp xu hướng hiện đại.
                    </h4>
                    <div class="service-title">
                        <p>
                            Đội ngũ nhân viên chúng tôi có đủ năng lực để xây dựng trang chủ, trung tâm mua sắm trực tuyến,… với các giao diện máy tính, giao diện điện thoại theo tiêu chuẩn web dành cho các doanh nghiệp, cơ quan, đoàn thể ở các lĩnh vực khác nhau.
                            Không chỉ dừng lại ở việc thiết kế, chúng tôi còn có thể trực tiếp sản xuất theo hình thức OEM,ODM các sản phẩm bắt kịp xu hướng hiện đại, đáp ứng nhu cầu của khách hàng.Chúng tôi sở hữu những ý tưởng sáng tạo và giá trị.
                        </p>
                    </div>
                    <div class="service-tag">
                        <p>BRANDING / WEBSITE / APP /</p>
                    </div>
                </div>
                <div class="col-md-5 col-sm-6">
                    <img class="service-img" src="/content/images/publics/service-01.png" />
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>    

@endsection

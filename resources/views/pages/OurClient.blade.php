@extends('layouts.layout_web')

@section('title', 'LIBEROVINA CO.,LTD')

@push('css')
    <link href="{{ URL::to('/') }}/content/css/ourclient.css" rel="stylesheet">
@endpush

@section('content')
<div class="main-slide-item">
        
    </div>
    <div class="client-body container">
        <div class="">
            <div class="contact-header col-md-6 col-sm-9">
                <div class="header">
                    <h5>danh sách</h5>
                    <h1>
                        Đối tác tiêu biểu
                    </h1>
                </div>
                <div class="content">
                    <div>
                        Bất cứ khi nào, bất cứ nơi đâu, công ty Liberovina sẽ luôn đồng hành cùng bạn.
                        Bằng cách cung cấp các sản phẩm cạnh tranh và các dịch vụ khác biệt cho các công ty toàn cầu lẫn các công ty hàng đầu trong nước, chúng tôi bảo đảm sẽ là một đối tác tốt của bạn.
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="">
            <div class="col-sm-12">
                <div class="row">
                    <div class="block-slider">
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Allianz.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Amorepacific.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/audi.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Bankofkorea.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/bmw.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/client01.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/GMkorea.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/GS.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/hanwha.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Hollyscoffee.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Homeplus.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/huyndai.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Kakao.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Kanu.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/KBS.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Kia.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Koreanair.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/LG.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/LIG.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Line.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Lotte.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Metlife.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Modetour.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/MrPizza.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Naver.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/posco.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Prudential.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/samsung.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Soil.png" />
                        </div>
                        <div class="slider-item col-md-2 col-sm-3 col-xs-4">
                            <img src="/content/images/publics/Logos/Ssangyong.png" />
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <br />
            <br />
        </div>
    </div>    


@endsection

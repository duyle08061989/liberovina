@extends('layouts.layout_web')

@section('title', 'LIBEROVINA CO.,LTD')

@push('css')
    <link href="{{ URL::to('/') }}/plugins/sweetalert/dist/sweetalert.css" rel="stylesheet" />
    <link href="{{ URL::to('/') }}/slick/slick.css" rel="stylesheet">
    <link href="{{ URL::to('/') }}/slick/slick-theme.css" rel="stylesheet">
    <link href="{{ URL::to('/') }}/content/css/ourclient.css" rel="stylesheet">
    <link href="{{ URL::to('/') }}/content/css/contact.css" rel="stylesheet">
@endpush

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

@section('content')
<div class="main-slide-item">
        <div class="block-breadcrumb">
            <div class="container">
                <div class="row">
                    <ul class="breadcrumb">
                        <li>
                            <a href="/">Trang chủ</a>
                        </li>
                        <li>
                            Dịch vụ
                        </li>
                        <li class="active">
                            Business To Business 
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="slide-item-bg">
            <img src="/content/images/publics/Banners/banner-pro-kts.png" />
        </div>
        <div class="container">
            <div class="">
                <div class="slide-desc-outer">
                    <div class="slide-desc">
                        <h4><span style="font-family: Lato-Light, Arial, sans-serif;text-transform:uppercase;color:#fff;font-size:16px;">cung cấp giải pháp tốt hơn</span><span style="font-family: Lato-Bold, Arial, sans-serif;text-transform:uppercase;font-size:30px;color:#b8a27e"> cho các công ty</span></h4>
                        <h5><span style="font-family: Lato-Black,Arial,sans-serif;text-transform:uppercase;font-size:48px;color:#fff">thương mại điện tử</span></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="client-body container">
        <div class="">
            <div class="contact-header col-md-6 col-sm-9">
                <div class="header">
                    <h5>Dịch Vụ Của Chúng Tôi</h5>
                    <h1>
                        Business To Business
                    </h1>
                </div>
                <div class="content">
                    <div>
                        Dành cho mục đích làm quà tặng hay khách hàng mua sỉ số lượng lớn.
                        Tùy theo mục đích mua hàng, chúng tôi bảo đảm sẽ làm hài lòng quý khách hàng bằng dịch vụ, sản phẩm mới lại của công ty.
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="">
            <div class="col-sm-12">
                <div class="row">
                    <div class="block-slider">
                        <div class="img-item col-md-3 col-sm-4 col-xs-6" index="0">
                            <img src="/content/images/publics/b2b/chubb.tui.png" />
                        </div>
                        <div class="img-item col-md-3 col-sm-4 col-xs-6" index="1">
                            <img src="/content/images/publics/b2b/chubb-printing-mockup-01.jpg" />
                        </div>
                        <div class="img-item col-md-3 col-sm-4 col-xs-6" index="2">
                            <img src="/content/images/publics/b2b/sharp-1.jpg" />
                        </div>
                        <div class="img-item col-md-3 col-sm-4 col-xs-6" index="3">
                            <img src="/content/images/publics/b2b/sharp-2.jpg" />
                        </div>
                        <div class="img-item col-md-3 col-sm-4 col-xs-6" index="4">
                            <img src="/content/images/publics/b2b/orange.png" />
                        </div>
                        <div class="img-item col-md-3 col-sm-4 col-xs-6" index="5">
                            <img src="/content/images/publics/b2b/gray.png" />
                        </div>
                        <div class="img-item col-md-3 col-sm-4 col-xs-6" index="6">
                            <img src="/content/images/publics/b2b/pink.png" />
                        </div>
                        <div class="img-item col-md-3 col-sm-4 col-xs-6" index="7">
                            <img src="/content/images/publics/b2b/mint.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="clearfix"></div>
        <div>
            <div class="col-md-8 col-md-offset-2 col-sm-12">
                <div class="contact-mess-header">
                    <h2>LIÊN HỆ NGAY VỚI CHÚNG TÔI</h2>
                    <br />
                    <br />
                    <br />
                </div>
                <form class="form-contact form-horizontal" method="post">
                    @csrf
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" id="Name" name="Name" class="form-control" placeholder="Họ tên / Công ty" required />
                                </div>
                                <div class="form-group">
                                    <input type="tel" id="Phone" name="Phone" class="form-control" placeholder="Số điện thoại" required />
                                </div>
                                <div class="form-group">
                                    <input type="email" id="Email" name="Email" class="form-control" placeholder="Email" required />
                                </div>
                                <div class="form-group">
                                    <input type="text" id="Address" name="Address" class="form-control" placeholder="Địa chỉ" required />
                                </div>
                                <div class="form-group">
                                    <input type="text" id="Skype" name="Skype" class="form-control" placeholder="Skype / Hangout / Zalo" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <textarea id="Message" name="Message" class="form-control" rows="12" placeholder="Mặt hàng / Số lượng / Nội dung ...." required></textarea>
                                <br>
                                <div class="pull-right">
                                    <div class="g-recaptcha" data-sitekey="6LcnlqYZAAAAAFksy27tJYl5SWA7fiMiMdo6G1Yj"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="bt-send read-more wwa-read-more"><span class="plush">+</span><span class="bt-text">Gửi đi</span></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
    <br />
    </div>

    <div class="modal modal-slider-image" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="slider-list">
                    <div class="slider-item col-md-12" index="0">
                        <div class="slider-item-img col-md-12">
                            <img src="/content/images/publics/b2b/original/chubb.tui.png" />
                        </div>
                        {{-- <div class="slider-item-content col-md-6">
                            <ul>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                            </ul>
                        </div> --}}
                    </div>
                    <div class="slider-item" index="1">
                        <div class="slider-item-img col-md-12">
                            <img src="/content/images/publics/b2b/original/chubb-printing-mockup-01.jpg" />
                        </div>
                        {{-- <div class="slider-item-content col-md-6">
                            <ul>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                            </ul>
                        </div> --}}
                    </div>
                    <div class="slider-item" index="2">
                        <div class="slider-item-img col-md-12">
                            <img src="/content/images/publics/b2b/original/sharp-1.jpg" />
                        </div>
                        {{-- <div class="slider-item-content col-md-6">
                            <img src="/content/images/publics/b2b/original/sharp-2.jpg" />
                        </div> --}}
                    </div>
                    <div class="slider-item" index="3">
                        <div class="slider-item-img col-md-12">
                            <img src="/content/images/publics/b2b/original/sharp-2.jpg" />
                        </div>
                    </div>
                    <div class="slider-item" index="4">
                        <div class="slider-item-img col-md-6">
                            <img src="/content/images/publics/b2b/original/cover-1.jpg" />
                        </div>
                        <div class="slider-item-content col-md-6">
                            <img src="/content/images/publics/b2b/original/cover-2.jpg" />
                        </div>
                        
                        {{-- <div class="slider-item-content col-md-6">
                            <ul>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                            </ul>
                        </div> --}}
                    </div>
                    <div class="slider-item" index="5">
                        <div class="slider-item-img col-md-12">
                            <img src="/content/images/publics/b2b/original/gray.png" />
                        </div>
                        {{-- <div class="slider-item-content col-md-6">
                            <ul>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                            </ul>
                        </div> --}}
                    </div>
                    <div class="slider-item" index="6">
                        <div class="slider-item-img col-md-12">
                            <img src="/content/images/publics/b2b/original/pink.png" />
                        </div>
                        {{-- <div class="slider-item-content col-md-6">
                            <ul>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                            </ul>
                        </div> --}}
                    </div>
                    <div class="slider-item" index="7">
                        <div class="slider-item-img col-md-12">
                            <img src="/content/images/publics/b2b/original/mint.png" />
                        </div>
                        {{-- <div class="slider-item-content col-md-6">
                            <ul>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                                <li>content</li>
                            </ul>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ URL::to('/') }}/plugins/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ URL::to('/') }}/slick/slick.min.js"></script>
    <script src="{{ URL::to('/') }}/content/js/home.js"></script>
    <script>
        (function($){
            $(document).ready(function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').val()
                    }
                });


                $(".block-slider").on('click', '.img-item', function(){
                    n = $(this).attr("index");
                    $('.slider-list').slick('slickGoTo',n)
                    $(".modal-slider-image").modal();
                });
        
                var owlVN = $('.slider-list').slick({
                    arrows: true,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    /* fade: true, */
                    autoplay: false,
                    speed:500,
                });

                function sendRequest(params){
                    $.ajax({
                        url: "{{route("api.contact.b2b")}}",
                        type: 'POST',
                        data: {params},
                        dataType: 'text',
                        success: function (data) {
                            if(data == "false"){
                                swal("{{__('Error')}}", "{{__('Please try again')}}!", "error");
                                return;
                            }
                            else{
                                swal("{{__('Success')}}", "{{__('We will contact you as soon as possible')}}!", "success");
                                $(".sweet-alert").find("button.confirm").removeAttr('disabled');
                                grecaptcha.reset();
                                formRequest.trigger("reset");
                            }
                        },
                        error: function (err) {
                            swal("{{__('Error')}}", "{{__('Please try again later')}}!", "error");
                            return false;
                        }
                    });
                }

                var formRequest = $(".form-contact");
                formRequest.on("submit",function(e){
                    e.preventDefault();

                    var recaptcha = $("#g-recaptcha-response").val();
                    if (recaptcha === "") {
                        event.preventDefault();
                        alert("Please check the recaptcha");
                        return;
                    }

                    var name = formRequest.find("#Name").val();
                    var email = formRequest.find("#Email").val();
                    var phone = formRequest.find("#Phone").val();
                    var address = formRequest.find("#Address").val();
                    var skype = formRequest.find("#Skype").val();
                    var message = formRequest.find("#Message").val();
                    var captcha = recaptcha;
                    var params = {
                        'name' : name,
                        'email' : email,
                        'phone' : phone,
                        'address' : address,
                        'skype' : skype,
                        'message' : message,
                        'g-recaptcha-response' : captcha,
                    };
                    swal({
                        title: "{{__('Do You Want To Send This Request')}}?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonText: "{{__('Cancel')}}",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "{{__('Submit')}}",
                        closeOnConfirm: false
                    },
                    function(isConfirm){
                        if (isConfirm) {
                            $(".sweet-alert").find("button.confirm").prop('disabled', true);
                            sendRequest(params);
                        } else {
                            return;
                        }    
                    });  
                });
            });
        })(jQuery);  
    </script>
@endpush
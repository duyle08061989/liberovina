@extends('layouts.layout_web')

@section('title', 'LIBEROVINA CO.,LTD')

@push('css')
    <link href="{{ URL::to('/') }}/content/css/contact.css" rel="stylesheet">
@endpush

@section('content')
<div class="body-content">
        <div class="main-slide-item">
            
        </div>
        <div class="contact-body container">
            <div class="row">
                <div class="contact-header col-md-7 col-sm-9">
                    <div class="header">
                        <h5>thông tin</h5>
                        <h1>
                            Liên hệ
                        </h1>
                    </div>
                    <div class="content">
                        <div>
                            Bất cứ khi nào, bất cứ nơi đâu, công ty Liberovina sẽ luôn đồng hành cùng bạn.
                            Bằng cách cung cấp các sản phẩm cạnh tranh và các dịch vụ khác biệt cho các công ty toàn cầu lẫn các công ty hàng đầu trong nước, chúng tôi bảo đảm sẽ là một đối tác tốt của bạn.
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-address">
                <div class="row">
                    <div class="col-md-7 col-sm-12">
                        <div class="row">
                            <div class="col-sm-7 col-xs-12">
                                <i class="fa fa-map-marker"></i>
                                <p>P7-37.OT12B-15, TÒA NHÀ PARK 7 VINHOMES CENTRAL PARK,</p>
                                <p>20A ĐIỆN BIÊN PHỦ, PHƯỜNG 22,</p>
                                <p>Q. Bình Thạnh, TP.HCM</p>
                            </div>
                            <div class="col-sm-5 col-xs-12">
                                <i class="fa fa-phone"></i>
                                <p>19000 68899</p>
                                <p>info@liberovina.vn</p>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <br />
                <div id="map_canvas" class="block-map col-sm-12">
    
                </div>
            </div>
            <div class="contact-mess">
                <div class="contact-mess-header">
                    <h2>LỜI NHẮN CỦA QUÝ KHÁCH</h2>
                    <br />
                    <br />
                    <br />
                </div>
                <form class="form-contact form-horizontal" method="post">
                    @csrf
                    <div class="col-md-7 col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" id="Name" name="Name" class="form-control" placeholder="Họ tên" required />
                                </div>
                                <div class="form-group">
                                    <input type="tel" id="Phone" name="Phone" class="form-control" placeholder="Số điện thoại" required />
                                </div>
                                <div class="form-group">
                                    <input type="text" id="Address" name="Address" class="form-control" placeholder="Địa chỉ" required />
                                </div>
                                <div class="form-group">
                                    <input type="email" id="Email" name="Email" class="form-control" placeholder="Email" required />
                                </div>
                                <div class="form-group">
                                    <input type="text" id="Skype" name="Skype" class="form-control" placeholder="Skype / Hangout / Zalo" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <textarea id="Content" name="Content" class="form-control" rows="12" placeholder="Nội dung" required></textarea>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="bt-send read-more wwa-read-more"><span class="plush">+</span><span class="bt-text">Gửi đi</span></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script async defer type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyAGuDFaJF5mpxZZsKh-iz7bL4B7bAm9yno&libraries=places"></script>
    <script src="{{ URL::to('/') }}/content/formValidation/message.js"></script>
    <script src="{{ URL::to('/') }}/content/formValidation/formValidation.min.js"></script>
    <script src="{{ URL::to('/') }}/content/formValidation/formValidation.bootstap.min.js"></script>
    <script>
        (function($){
            $(document).ready(function(){
                var interval_obj = setInterval(function () {
                if (google.maps != "undefined"){
                    /*google map*/
                    var myLatlng = new google.maps.LatLng(10.7955482, 106.7190542);
                    var mapOptions = {
                        zoom: 16,
                        center: myLatlng
                    };
                    var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
                    var contentString = "";
                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        title: 'Libero Vina office',
                    });
                    clearInterval(interval_obj);
                }
            },1000);

            var langID = 1;
            var isSend = 0;
            //form validation
            var currForm = $(".form-contact");
            if(langID == 1){
                currForm
                 .formValidation({
                     framework: 'bootstrap',
                     icon: {
                         valid: 'glyphicon glyphicon-ok',
                         invalid: 'glyphicon glyphicon-remove',
                         validating: 'glyphicon glyphicon-refresh'
                     },
                     fields: {
                         Name: {
                             validators: {
                                 notEmpty: {
                                     message: "Vui lòng nhập Họ Tên của bạn."
                                 }
                             }
                         },
                         Phone: {
                             validators: {
                                 notEmpty: {
                                     message: "Vui lòng nhập Số điện thoại của bạn."
                                 },
                                 phone: {
                                     message: 'Số điện thoại không hợp lệ.'
                                 }
                             }
                         },

                         Address: {
                             validators: {
                                 notEmpty: {
                                     message: "Vui lòng nhập Địa chỉ của bạn."
                                 }
                             }
                         },
                         Content: {
                             validators: {
                                 notEmpty: {
                                     message: "Vui lòng nhập Nội dung bạn muốn gửi."
                                 }
                             }
                         },
                         Email: {
                             validators: {
                                 notEmpty: {
                                     message: "Vui lòng nhập Email của bạn."
                                 },
                                 emailAddress: {
                                     message: 'Email không hợp lệ.'
                                 }
                             }
                         },
                     }
                 })
            }
            else{
                currForm
                     .formValidation({
                         framework: 'bootstrap',
                         icon: {
                             valid: 'glyphicon glyphicon-ok',
                             invalid: 'glyphicon glyphicon-remove',
                             validating: 'glyphicon glyphicon-refresh'
                         },
                         fields: {
                             Name: {
                                 validators: {
                                     notEmpty: {
                                         message: "Must not be empty!"
                                     }
                                 }
                             },
                             Phone: {
                                 validators: {
                                     notEmpty: {
                                         message: "Must not be empty!"
                                     },
                                     phone: {
                                         message: 'The value is not a valid phone number'
                                     }
                                 }
                             },

                             Address: {
                                 validators: {
                                     notEmpty: {
                                         message: "Must not be empty!"
                                     }
                                 }
                             },
                             Content: {
                                 validators: {
                                     notEmpty: {
                                         message: "Must not be empty!"
                                     }
                                 }
                             },
                             Email: {
                                 validators: {
                                     notEmpty: {
                                         message: "Must not be empty!"
                                     },
                                     emailAddress: {
                                         message: 'The value is not a valid email address'
                                     }
                                 }
                             },
                         }
                     })
            }

            if(isSend == 1){
                if(langID == 1){
                    alert("Cảm ơn bạn đã để lại thông tin. Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất!");
                }
                else{
                    alert("Thanks for your comment!");
                }
            }else if(isSend == -1){
                if(langID == 1){
                    alert("Đã có lỗi xảy ra. Xin vui lòng thử lại.");
                }
                else{
                    alert("Something went wrong. Please try again later.");
                }
            }
            });
        })(jQuery);  
    </script>
@endpush
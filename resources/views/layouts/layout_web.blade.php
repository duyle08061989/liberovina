<!DOCTYPE html>
<html>
<head>
	@include('includes.web.head')
</head>

<body>

	@include('includes.web.header')

	@yield('content')

	@include('includes.web.footer')

	<script src="{{ URL::to('/') }}/scripts/jquery-3.0.0.min.js"></script>
	<script src="{{ URL::to('/') }}/scripts/jquery-migrate-3.0.0.min.js"></script>
	<script src="{{ URL::to('/') }}/scripts/bootstrap.min.js"></script>
	<script src="{{ URL::to('/') }}/scripts/respond.min.js"></script>
	<script src="{{ URL::to('/') }}/content/slick/slick.min.js"></script>
	<script src="{{ URL::to('/') }}/content/js/layout.js"></script>
	<script>
		(function($){
			var marginLeft = $(".container-get-margin").css("margin-left");
            $(".main-slider-outer").find(".slick-dots").addClass("container").css("left", parseFloat(marginLeft) + "px");
			/* Open */
            $(document).on("click", ".open-nav-btn", function () {
                $("#myNav").css({ "height": "100%" });
                $(".nav-content").addClass("nav-fixed");
                $(this).find(".icon-bt").html('&#x2716;').css("color","#fff");
                $(this).addClass("close-nav-btn").removeClass("open-nav-btn");
            });

            $(document).on("click", ".close-nav-btn", function () {
                $(".nav-content").removeClass("nav-fixed");
                $("#myNav").css({ "height": "0%" });
                $(this).find(".icon-bt").html('');
                $(this).addClass("open-nav-btn").removeClass("close-nav-btn");
            });

            $(window).scroll(function () {
                var heightHeader = $("header").height();
                if ($(this).scrollTop() > heightHeader) {
                    $("header").addClass("fixed");
                }
                else {
                    $("header").removeClass("fixed");
                }
            });
		})(jQuery);
	</script>
	@stack('scripts')
</body>
</html>

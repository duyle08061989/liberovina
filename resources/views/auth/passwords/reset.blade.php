@extends('layouts.layout_web')
@section('title', 'Khôi Phục Mật Khẩu')

{{-- @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                
            </div>
        </div>
    </div>
</div>
@endsection --}}


@section('content')
<!--Sub Banner Wrap Start-->
<div class="sub-banner">
    <div class="container">
        <h6>Khôi Phục Mật Khẩu</h6>
    </div>
</div>
<!--Sub Banner Wrap End-->
<!--Main Content Wrap Start-->
<br>
<br>
<div class="kode_content_wrap" style="margin: 60px">
    <section>
        <div class="container">
            <div class="row">
                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" style="color:#fff" class="col-md-2 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" style="color:#fff" class="col-md-2 col-form-label text-md-right">{{ __('Mật Khẩu Mới') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" style="color:#fff" class="col-md-2 col-form-label text-md-right">{{ __('Xác Nhận Mật Khẩu') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Cập Nhật Mật Khẩu') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<!--Main Content Wrap End-->
@endsection

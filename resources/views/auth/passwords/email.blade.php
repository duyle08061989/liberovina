@extends('layouts.layout_web')
@section('title', 'Khôi Phục Mật Khẩu')

@section('content')
<!--Sub Banner Wrap Start-->
<div class="sub-banner">
    <div class="container">
        <h6>Khôi Phục Mật Khẩu</h6>
    </div>
</div>
<!--Sub Banner Wrap End-->
<!--Main Content Wrap Start-->
<br>
<br>
<div class="kode_content_wrap" style="margin: 60px">
    <section>
        <div class="container">
            <div class="row">
                <div class="card">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
    
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
    
                            <div class="form-group row">
                                <label for="email" style="color:#fff" class="col-md-2 col-form-label text-md-right">{{ __('Email của bạn') }}</label>
    
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
    
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row mb-0">
                                <div class="col-md-6 col-md-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Gửi Link Khôi Phục Mật Khẩu') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--Main Content Wrap End-->
@endsection

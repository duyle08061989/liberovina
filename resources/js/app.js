/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Router from './routes.js';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('articles', require('./components/articles.vue').default);

Vue.component('home', require('./components/home.vue').default);
Vue.component('students', require('./components/students.vue').default);
Vue.component('create_student', require('./components/create_student.vue').default);
Vue.component('edit_student', require('./components/edit_student.vue').default);
Vue.component('delete_student', require('./components/delete_student.vue').default);
Vue.component('search_student', require('./components/search_student.vue').default);
Vue.component('vue-pagination', require('./components/pagination.vue').default);
Vue.component('Modal', require('./components/modal.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router:Router
});

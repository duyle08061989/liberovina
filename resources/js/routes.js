import VueRouter from 'vue-router';
import Vue from 'vue';
import Home from './components/home.vue';
import Students from './components/students.vue';
import CreateStudent from './components/create_student.vue';
import EditStudent from './components/edit_student.vue';
/* import DeleteStudent from './components/delete_student.vue'; */
import SearchStudent from './components/search_student.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    routes:[
        { path: '/vue', component: Home },
        { path: '/vue/students', component: Students },
        { path: '/vue/createstudent', component: CreateStudent },
        { path: '/vue/editstudent/:id', component: EditStudent },
       /*  { path: '/vue/deletestudent/:id', component: DeleteStudent }, */
        { path: '/vue/searchstudent/:searchstring', component: SearchStudent }
    ]
})

export default router;
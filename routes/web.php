<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');

Route::get('/Home/ProductSlidePartial', 'HomeController@productSlidePartial');

Route::get('/WhoWeAre', 'HomeController@WhoWeAre')->name('WhoWeAre');

Route::get('/OurService', 'HomeController@OurService')->name('OurService');
Route::get('/OurProduct/Diary', 'HomeController@Diary')->name('Diary');
Route::get('/OurProduct/DeskAccessories', 'HomeController@DeskAccessories')->name('DeskAccessories');
Route::get('/OurProduct/BagWallet', 'HomeController@BagWallet')->name('BagWallet');
Route::get('/OurProduct/LeatherITAccessories', 'HomeController@LeatherITAccessories')->name('LeatherITAccessories');
Route::get('/OurProduct/LeatherCarAccessories', 'HomeController@LeatherCarAccessories')->name('LeatherCarAccessories');
Route::get('/OurProduct/FoldableTravelGoods', 'HomeController@FoldableTravelGoods')->name('FoldableTravelGoods');
Route::get('/OurProduct/Gift', 'HomeController@Gift')->name('Gift');

Route::get('/OurClient', 'HomeController@OurClient')->name('OurClient');
Route::get('/Recruitment', 'HomeController@Recruitment')->name('Recruitment');
Route::get('/Contact', 'HomeController@Contact')->name('Contact');

Route::get('/b2b', 'HomeController@b2b')->name('B2b');

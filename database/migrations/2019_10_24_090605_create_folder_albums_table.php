<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFolderAlbumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folder_albums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('folder_id');
            $table->bigInteger('album_id');
            $table->string('album_name',250);
            $table->string('album_cover',500);
            $table->string('album_ascii',250);
            $table->string('artist_name',250);
            $table->string('category_name',250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folder_albums');
    }
}

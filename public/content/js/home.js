﻿(function ($) {
    var activeTabSlide = function () {
        var tabID = $(".lst-ctg").find("li.active").attr("tab-id");
        $.ajax({
            url: '/Home/ProductSlidePartial',
            data: { type: tabID },
            type: 'GET',
            dataType : 'html',
            cache: false,
            success: function (result) {
                $('.tabcontent').html(result);
                $('.tabcontent').find(".slide-pro").slick({
                    arrows: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    fade: true,
                    cssEase: 'linear',
                    autoplay: true,
                    autoplaySpeed: 5000,
                    speed: 500,
                    dots: true
                });
            }
        });
    };
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val()
            }
        });

        $(".main-slide").slick({
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            fade: true,
            cssEase: 'linear',
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 500,
            dots: true,
            //          responsive: [
            //{
            //    breakpoint: 768,
            //    settings: {
            //        dots: true
            //    }
            //}
            //          ]
        });
        $(".tmn-slide").slick({
            arrows: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            cssEase: 'linear',
            autoplay: true,
            autoplaySpeed: 5000,
            speed: 500,
            dots: false,
            responsive: [
              {
                  breakpoint: 768,                  
                  settings: {
                      arrows: false,
                      slidesToShow: 2,
                      dots:true,
                  }
              },
              {
                  breakpoint: 420,
                  settings: {
                      slidesToShow: 1,
                      arrows: false,
                      dots: true,
                  }
              },
            ]
        });

        var marginArrows = $(".col-md-offset-1").css("margin-left");
        $(".tmn-slide").find(".slick-prev").css("left", "-" + marginArrows);
        $(".tmn-slide").find(".slick-next").css("right", "-" + marginArrows);

        activeTabSlide();

        $(".lst-ctg").on("click", "li", function () {
            $(".lst-ctg").find("li").removeClass("active");
            var $this = $(this);
            $this.addClass("active");

            activeTabSlide();
        });

    });
})(jQuery);
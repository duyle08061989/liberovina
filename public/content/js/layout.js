﻿(function ($) {
    $(document).ready(function () {
        $(".selectpicker").selectpicker();
        $(document).on("click", ".button-toggle-menu", function () {
            $(".lst-nav").slideToggle("slow");
        });
        $(".open-sidenav").click(function () {
            $("#my-sidebar").css('width', '250px');
        });
        $(".closebtn").click(function () {
            $("#my-sidebar").css('width', '0');
        });
        $(window).scroll(function () {
            if ($(this).scrollTop() > 1) {
                $(".header-outer").addClass("header-fix");
            }
            else {
                $(".header-outer").removeClass("header-fix");
            }
        });
    });
})(jQuery);
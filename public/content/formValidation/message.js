﻿function errorCode(value) {
    var str = "";
    switch (value) {
        case 1:
            str = "Cần nhập thông tin này.";
            break;
        case 2:
            str = "Từ 6 đến 30 ký tự";
            break;
        case 3:
            str = "Chỉ được nhập chữ cái, và số. Không chứa ký tự đặc biệt ngoại trừ dấu chấm và dấu gạch dưới.";
            break;
        case 4:
            str = "Không chứa ký tự đặc biệt trừ dấu chấm và dấu gạch dưới.";
            break;
        case 5:
            str = "Tên tài khoản đã được đăng ký.";
            break;
        case 6:
            str = "Địa chỉ email không hợp lệ.";
            break;
        case 7:
            str = "Địa chỉ email đã được đăng ký.";
            break;
        case 8:
            str = "Xác nhận mật khẩu không đúng.";
            break;
        case 9:
            str = "5 ký tự.";
            break;
        case 10:
            str = "Tối đa 15 ký tự.";
            break;
        case 11:
            str = "Cần nhập số điện thoại.";
            break;
        case 12:
            str = "Chứng minh thư đang được sử dụng bởi tài khoản khác.";
            break;
        case 13:
            str = "XĂ¡c nháº­n MĂ£ game khĂ´ng Ä‘Ăºng.";
            break;
        case 14:
            str = "Độ dài email ít hơn 201 ký tự.";
            break;
        case 15:
            str = "Số điện thoại bảo vệ được sử dung bởi tài khoản khác.";
            break;
        case 16:
            str = "Số diện thoại chưa chính xác.";
            break;
        case 17:
            str = "Mã captcha chưa chính xác.";
            break;
        case 18:
            str = "Xác nhận Email không đúng";
            break;
        case 19:
            str = "Chưa xác nhận";
            break;
        case 20:
            str = "Ít nhất 4 ký tự bao gồm chữ cái, và số.";
            break;
    }
    return str;

}
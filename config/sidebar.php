<?php

return [

    /*
    |--------------------------------------------------------------------------
    | View Storage Paths
    |--------------------------------------------------------------------------
    |
    | Most templating systems load templates from disk. Here you may specify
    | an array of paths that should be checked for your views. Of course
    | the usual Laravel view path has already been registered for you.
    |
    */

    'menu' => [[
		'icon' => 'fa fa-th-large',
		'title' => 'Trang Chủ',
		'url' => '/bce4821167bd3c68699e49342d2f946f71ec2e3df55c6b6fda9d411ef2fcfed1',
		'caret' => false
	],
	[
		'icon' => 'fa fa-th-large',
		'title' => 'Danh Sách Album',
		'url' => '/bce4821167bd3c68699e49342d2f946f71ec2e3df55c6b6fda9d411ef2fcfed1/albums'
	],
	[
		'icon' => 'fa fa-th-large',
		'title' => 'Tài Khoản',
		'url' => '/bce4821167bd3c68699e49342d2f946f71ec2e3df55c6b6fda9d411ef2fcfed1/employer-edit'
	]],
];

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dataEmail)
    {
        $subject = "Liên hệ B2B [LIBEROVINA]";
        $this->from($address = 'cskh@liberovina.vn', $name = 'LIBERO VINA');
        $this->subject($subject);
        $this->data = $dataEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('includes.mail.contact')->with([
            'data' => $this->data,
          ]);;
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Mail\Contact;

class CommonController extends Controller
{
    public function contactB2b(Request $request){
        try{
            $requests = $request->all();
            $params = $requests['params'];

            $recaptcha_secret = "6LcnlqYZAAAAAKhryT3EWYPvH-AbYo1IG_n9hwoI";  //khóa bí mật tùy theo project
            $recaptcha_response = $params['g-recaptcha-response'];  // token xác thực từ client gửi về
     
            $veri = curl_init();  //hàm khởi tạo
     
            $captcha_url = "https://www.google.com/recaptcha/api/siteverify";  //url xác thực của google
            //các câu lệnh set option cho biến xác thực
            curl_setopt($veri,CURLOPT_URL,$captcha_url);
            curl_setopt($veri,CURLOPT_POST,true);
            curl_setopt($veri,CURLOPT_POSTFIELDS,"secret=".$recaptcha_secret."&response=".$recaptcha_response);
            curl_setopt($veri,CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($veri,CURLOPT_RETURNTRANSFER,true);
    
            $recaptcha_output = curl_exec($veri); //thực thi và lấy kết quả
    
            curl_close($veri); //đóng 
    
            $decode_captcha = json_decode($recaptcha_output);  //dịch ngược json
    
            $captcha_status = $decode_captcha->success; //lấy giá trị key success
    
            if (!$captcha_status){
                return 'false';
            }

            $params['name'] = html_entity_decode($params['name']);
            $params['email'] = html_entity_decode($params['email']);
            $params['phone'] = html_entity_decode($params['phone']);
            $params['message'] = html_entity_decode($params['message']);
            $params['skype'] = html_entity_decode($params['skype']);
            $params['address'] = html_entity_decode($params['address']);
            $params['created_at'] = Carbon::now()->toDateTimeString();

            try{
                Mail::to([env('MAIL_FORWARD')])->send(new Contact($params));
                return 'true';
            }catch(Exception $e){
                return 'false';
            } 
            return 'false';
        }
        catch(Exception $e){
            return 'false';
        }
    }

}

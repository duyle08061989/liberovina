<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.index');
    }

    public function productSlidePartial(Request $request)
    {
        $type = $request->get("type");

        $options = view("pages.ProductSlide",compact('type'))->render();

        return $options;
    }

    public function WhoWeAre()
    {
        return view('pages.WhoWeAre');
    }
    public function OurService()
    {
        return view('pages.OurService');
    }
    public function Diary()
    {
        return view('pages.Diary');
    }
    public function LeatherITAccessories()
    {
        return view('pages.LeatherITAccessories');
    }
    public function BagWallet()
    {
        return view('pages.BagWallet');
    }
    public function LeatherCarAccessories()
    {
        return view('pages.LeatherCarAccessories');
    }
    public function FoldableTravelGoods()
    {
        return view('pages.FoldableTravelGoods');
    }

    public function Gift()
    {
        return view('pages.Gift');
    }
    public function OurClient()
    {
        return view('pages.OurClient');
    }
    public function Recruitment()
    {
        return view('pages.Recruitment');
    }
    public function Contact()
    {
        return view('pages.Contact');
    }
    public function DeskAccessories()
    {
        return view('pages.DeskAccessories');
    }
    public function b2b()
    {
        return view('pages.B2B');
    }

}
    
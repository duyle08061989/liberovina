<?php

namespace App\Http\Middleware;

use Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class CustomCKFinderAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function checkAdmin()
    {
        if (Auth::guard("admin")) {
            return true;
        }
        return false;
    }
}

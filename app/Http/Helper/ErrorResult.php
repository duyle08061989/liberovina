<?php

namespace App\Http\Helper;

class ErrorResult{

    protected $_error = false;
    protected $_errorMsg = null;
    protected $_data = null;    
    
    public function DataResult($error,$errorMsg,$data){
        $this->_error = $error;
        $this->_errorMsg = $errorMsg;
        $this->_data = $data;
              
        $result = array_add(['error'=>$this->_error],'errorMsg',$this->_errorMsg,'data',$this->_data);
        return $result;
    }

    public function getError(){
        return $this->_error;
    }
    public function getErrorMsg(){
        return $this->_errorMsg;
    }
    public function getData(){
        return $this->_data;
    }
}
?>
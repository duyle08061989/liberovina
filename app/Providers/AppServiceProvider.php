<?php

namespace App\Providers;

use App\Models\Album;
use App\Models\Song;
use App\Models\AlbumComment;
use App\Models\SongComment;
use App\Models\Folder;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema; 

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		if($this->app->environment() === 'production'){
			\URL::forceScheme('https');
		}
        //
        Schema::defaultStringLength(200);

        view()->composer('includes.web.top-artist',function($view){
            $albumModel = new Album();
            $top_album_artist = $albumModel->getTopAlbumArtist(6);
            $view->with('data',$top_album_artist);
        });

        view()->composer('includes.web.top-song',function($view){
            $songModel = new Song();
            $topSong = $songModel->getTopSong(8);
            $view->with('data',$topSong);
        });

        view()->composer('includes.web.album-comment',function($view){
            $album_id = $view->getData()['album_id'];

            $album_comment_model = new AlbumComment();
            $comments = $album_comment_model->getCommentOfAlbum($album_id);
            $view->with(['data'=>$comments,'album_id'=>$album_id]);
        });

        view()->composer('includes.web.song-comment',function($view){
            $song_id = $view->getData()['song_id'];

            $song_comment_model = new SongComment();
            $comments = $song_comment_model->getCommentOfSong($song_id);
            $view->with(['data'=>$comments,'song_id'=>$song_id]);
        });

        view()->composer('includes.web.fav-button',function($view){
            $album_id = $view->getData()['album_id'];

            $folder_model = new Folder();       
            $folders = $folder_model->getAllFolderOfUser();
            $folder_model = new Folder();   
            $check_album = $folder_model->checkAlbumFavOfUser($album_id);
            $view->with(['folders'=>$folders,'check_album' => $check_album->getData(),'album_id'=>$album_id]);
        });

        view()->composer('includes.web.list-listening-album',function($view){
            $album_model = new Album();   
            
            $history = $album_model->getTopHistory();
            $view->with(['history'=>$history]);
        });
    }
}
